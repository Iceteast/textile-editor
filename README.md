# Textile Editor v0.1

## About

It's a simple editor written by PyQt5. One can draw tiles(parallelogram for now)
in 2D Scene.

## Environment

    PyCharm 2023.1.3 (Community Edition)
    Build #PC-231.9161.41, built on June 20, 2023
    Runtime version: 17.0.7+10-b829.16 amd64
    VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
    Windows 11.0
    GC: G1 Young Generation, G1 Old Generation
    Memory: 2020M
    Cores: 16

## Python Environment

    Python 3.9
    PyQt5 5.15.9

## Run

    ./Main.py

## TODO
    
- [x] Output (**Need to know the format**)
- [x] Mouse Click (give 5 sliders for each tile is rly dummy)
- [ ] Maybe 3D Scene or drag-draw (I don't think it's supported)
- [ ] Add commandline parameter parser
- [ ] Save window
- [ ] Options window