#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import math
from abc import abstractmethod

from PyQt5.QtWidgets import QGraphicsPathItem

from cfg import *


# Author by Alex

class Drawable(QGraphicsPathItem):
    """
    Interface for all possible drawn items.
    """

    def __init__(self, parent):
        super(Drawable, self).__init__()

        self.statbar = parent.statbar
        self.parent = parent

    @abstractmethod
    def mousePressEvent(self, event):
        pass

    @abstractmethod
    def getPath(self):
        """
        :return: the List of Vertice, in order to draw it out.
        """
        pass


class Tile(Drawable):

    def __init__(self, parent, idShape, paths, color=None):
        super().__init__(parent)
        if color is None:
            color = [0, 0, 0]
        self.paths = paths
        self.color = color
        self.idShape = idShape

    def getPath(self):
        return self.paths

    def mousePressEvent(self, event):
        # print("AA" + str(self.parent.pattern.tile.width))
        self.statbar.showMessage("Shape " + str(self.idShape) + " is chosen")
        self.parent.textile.choose(self.idShape)
        self.parent.currentTile = self.idShape
        self.parent.drawMap()
        event.accept()


def isValid(config):
    """
    :param config: all parameters of a tile.
    :return: if a config is valid
    """
    coord, width, height, angle, color = config
    if coord[0] < COORDINATE_MIN or coord[0] > COORDINATE_MAX \
            or coord[1] < COORDINATE_MIN or coord[1] > COORDINATE_MAX \
            or coord[2] < COORDINATE_MIN or coord[2] > COORDINATE_MAX:
        return False
    if width < WIDTH_MIN or width > WIDTH_MAX:
        return False
    if height < HEIGHT_MIN or height > HEIGHT_MAX:
        return False
    if angle < ANGLE_MIN or angle > ANGLE_MAX:
        return False
    if color[0] < 0 or color[0] > 255 \
            or color[1] < 0 or color[1] > 255 \
            or color[2] < 0 or color[2] > 255:
        return False
    return True


class Shape:
    idShape = 1

    def __init__(self, coord=None, width=0, height=0, angle=0, color=None):
        if coord is None:
            coord = [0, 0, 0]
        if color is None:
            color = [0, 0, 0]
        self.coord = coord
        self.width = width
        self.height = height
        self.angle = angle
        self.color = color
        self.idShape = Shape.idShape
        Shape.idShape += 1

    def getAngle(self):
        return self.angle

    def setAngle(self, c):
        if ANGLE_MIN <= c <= ANGLE_MAX:
            self.angle = c

    def getWidth(self):
        return self.width

    def setWidth(self, c):
        if WIDTH_MIN <= c <= WIDTH_MAX:
            self.width = c

    def getHeight(self):
        return self.height

    def setHeight(self, c):
        if HEIGHT_MIN <= c <= HEIGHT_MAX:
            self.height = c

    def getX(self):
        return self.coord[0]

    def setX(self, c):
        if COORDINATE_MIN <= c <= COORDINATE_MAX:
            self.coord[0] = c

    def getY(self):
        return self.coord[1]

    def setY(self, c):
        if COORDINATE_MIN <= c <= COORDINATE_MAX:
            self.coord[1] = c

    def getConfig(self):
        return self.idShape, self.getPath(), self.color

    def getPath(self):
        paths = []
        x = self.coord[0]
        y = self.coord[1]

        paths.append([x, y])
        paths.append([x + self.width, y])
        paths.append([x + self.width + self.height * math.cos(math.radians(self.angle)), y + self.height])
        paths.append([x + self.height * math.cos(math.radians(self.angle)), y + self.height])
        return paths

    def updateConfig(self, config):
        coord, width, height, angle = config
        self.coord = coord
        self.width = width
        self.height = height
        self.angle = angle

    def toString(self):
        paths = self.getPath()
        res = "# --- Shape " + str(self.idShape) + " ---\n"
        for i in range(len(paths) - 1):
            res += "[" + str(paths[i][0]) + ", " + str(paths[i][1]) + "] - [" \
                   + str(paths[i + 1][0]) + ", " + str(paths[i + 1][1]) + "]\n"
        res += "[" + str(paths[len(paths) - 1][0]) + ", " + str(paths[len(paths) - 1][1]) + "] - [" \
               + str(paths[0][0]) + ", " + str(paths[0][1]) + "]\n\n"
        return res


class Textile:
    """
    A Textile includes a list of existed tiles and one tile to save the parameters to create a new tile.
    """

    def __init__(self, tiles):
        self.tiles = []
        for tile in tiles:
            if not isValid(tile):
                raise "Wrong config of Tiles!"
            coord, width, height, angle, color = tile
            self.tiles.append(Shape(coord, width, height, angle, color))

        self.tile = Shape()

    def addTile(self, coord, width, height, angle, color):
        if not isValid((coord, width, height, angle, color)):
            raise "Wrong config of Tiles!"
        self.tiles.append(Shape(coord, width, height, angle, color))

    def updateTile(self, idShape, coord, width, height, angle):
        if not isValid((coord, width, height, angle, [0, 0, 0])):
            raise "Wrong config of Tile!"
        for tile in self.tiles:
            if tile.idShape == idShape:
                tile.updateConfig((coord, width, height, angle))
                return
        raise "Can't find current Tile![" + str(idShape) + "]"

    def removeTile(self, idShape):
        for tile in self.tiles:
            if tile.idShape == idShape:
                self.tiles.remove(tile)
                break

    def choose(self, idShape):

        for tile in self.tiles:
            if tile.idShape == idShape:
                tile.color = CHOOSE_COLOR
                self.tile.updateConfig((tile.coord, tile.width, tile.height, tile.angle))
            else:
                tile.color = TILE_COLOR

    def save(self, filename="test.txt"):
        with open(filename, 'w') as f:
            for tile in self.tiles:
                f.write(tile.toString())
