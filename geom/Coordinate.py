# -*- coding:utf-8 -*-

# Author by Alex

class Coordinate:

    # coordinate in draw map. xy for 2D scene, xyz for 3D scene.
    def __init__(self, x, y, z):
        self.axisX = x
        self.axisY = y
        self.axisZ = z

    def getCoordinateXYZ(self):
        return self.axisX, self.axisY, self.axisZ

    def getCoordinate(self):
        return self.axisX, self.axisY
