#! /usr/bin/env python3
# -*- coding:utf-8 -*-
from PyQt5.QtGui import QColor


# Author by Alex

def isNumeric(string):
    try:
        float(string)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        for c in string:
            unicodedata.numeric(c)
        return True
    except ValueError:
        pass

    return False


def makeColor(color):
    r, g, b = color
    if 0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255:
        return QColor(r, g, b)
    return None


def doNothing(c):
    pass


if __name__ == '__main__':
    print("1.1 is a number " + str(isNumeric("1.1")))
    print("hello is a number " + str(isNumeric("he1lo")))
