#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

import sys

from PyQt5.QtCore import QTimer

from framework import MainMap

from cfg import *
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QHBoxLayout, QVBoxLayout, \
    QDesktopWidget, QMessageBox, QWidget

from framework.MySlider import MySlider


class Frame(QMainWindow):
    # main framework
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Textile Editor ' + VERSION)
        self.resize(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.statbar = self.statusBar()
        self.statbar.showMessage('Ready')
        self.setCenter()
        self.timer = QTimer()
        widget = QWidget()
        self.setCentralWidget(widget)

        hbox = QHBoxLayout()
        # setMainMap
        self.mainMap = MainMap.Main(self)
        hbox.addWidget(self.mainMap)

        # setMenu
        menu = QVBoxLayout()

        self.sliderA = MySlider("Angle",
                                self.mainMap.textile.tile.setAngle,
                                self.mainMap.textile.tile.getAngle,
                                ANGLE_MIN, ANGLE_MAX)
        self.sliderW = MySlider("Width",
                                self.mainMap.textile.tile.setWidth,
                                self.mainMap.textile.tile.getWidth,
                                WIDTH_MIN, WIDTH_MAX)
        self.sliderH = MySlider("Height",
                                self.mainMap.textile.tile.setHeight,
                                self.mainMap.textile.tile.getHeight,
                                HEIGHT_MIN, HEIGHT_MAX)
        self.sliderX = MySlider("XC",
                                self.mainMap.textile.tile.setX,
                                self.mainMap.textile.tile.getX,
                                COORDINATE_MIN, COORDINATE_MAX)
        self.sliderY = MySlider("YC",
                                self.mainMap.textile.tile.setY,
                                self.mainMap.textile.tile.getY,
                                COORDINATE_MIN, COORDINATE_MAX)
        menu.addWidget(self.sliderA)
        menu.addWidget(self.sliderW)
        menu.addWidget(self.sliderH)
        menu.addWidget(self.sliderX)
        menu.addWidget(self.sliderY)

        layoutButton = QHBoxLayout()
        btn_save = QPushButton("Save", self)
        btn_save.clicked.connect(lambda: self.mainMap.textile.save("a.txt"))
        btn_add = QPushButton("Add", self)
        btn_add.clicked.connect(
            lambda: self.mainMap.addTile([self.sliderX.slider.value(),
                                          self.sliderY.slider.value(), 0],
                                         self.sliderW.slider.value(),
                                         self.sliderH.slider.value(),
                                         self.sliderA.slider.value()))
        btn_upd = QPushButton("Update", self)
        btn_upd.clicked.connect(
            lambda: self.mainMap.updateTile(self.mainMap.currentTile,
                                            [self.sliderX.slider.value(),
                                             self.sliderY.slider.value(), 0],
                                            self.sliderW.slider.value(),
                                            self.sliderH.slider.value(),
                                            self.sliderA.slider.value()))
        layoutButton.addWidget(btn_save)
        layoutButton.addWidget(btn_add)
        layoutButton.addWidget(btn_upd)
        menu.addLayout(layoutButton)

        hbox.addLayout(menu)
        hbox.setStretchFactor(self.mainMap, 8)
        hbox.setStretchFactor(menu, 1)
        widget.setLayout(hbox)

        self.timer.timeout.connect(self.refresh)
        self.timer.start(300)

        self.show()

    def refresh(self):
        self.sliderW.refresh()
        self.sliderH.refresh()
        self.sliderX.refresh()
        self.sliderY.refresh()
        self.sliderA.refresh()

    # set Form im Center
    def setCenter(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, QCloseEvent):
        reply = QMessageBox.question(self,
                                     'Map Editor ' + VERSION,
                                     "Are you sure to exit ?",
                                     QMessageBox.No | QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Frame()
    sys.exit(app.exec_())
