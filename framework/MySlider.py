from PyQt5 import QtGui
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QHBoxLayout, QLabel, QSlider
import sys

from cfg import *
from utils import isNumeric


class MySlider(QWidget):
    """
    Slider suite,
    made by one slider one label and one text line
    """

    def __init__(self, label, reference, current, minValue=SLIDER_MIN, maxValue=SLIDER_MAX):
        """

        :param label: Context of Label in front
        :param reference: the function, which modifies the reference value.
        :param current: could be an Integer, or a function, which get the current value of reference.
        :param minValue: minimum of slider, default value in cfg.py
        :param maxValue: maximum of slider, default value in cfg.py
        """
        super(MySlider, self).__init__()

        self.slider = QSlider(Qt.Horizontal)
        self.text = QLineEdit()
        self.label = QLabel(label)
        self.setValue = reference
        self.current = current
        self.initUI(label, minValue, maxValue, self.refresh())

    def initUI(self, label, minValue, maxValue, current):
        self.setWindowTitle("test")
        self.resize(800, 500)

        """ old version
        layout = QVBoxLayout()
        self.label = QLabel(label)
        layout.addWidget(self.label)

        lowerLayout = QHBoxLayout()
        self.text = QLineEdit(str(current))
        self.text.setAlignment(Qt.AlignCenter)
        self.text.setMaximumWidth(28)
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(minValue, maxValue)
        self.slider.setValue(current)
        self.slider.setTickInterval(5)

        lowerLayout.addWidget(self.slider)
        lowerLayout.addWidget(self.text)
        lowerLayout.setStretchFactor(self.slider, 19)
        lowerLayout.setStretchFactor(self.text, 1)
        lowerLayout.setStretchFactor(self.text, 1)
        self.slider.valueChanged.connect(self._value)
        self.text.returnPressed.connect(self._update)

        layout.addLayout(lowerLayout)
        layout.setStretchFactor(self.label, 1)
        layout.setStretchFactor(lowerLayout, 9)
        """
        layout = QHBoxLayout()

        self.text.setText(str(current))
        self.text.setAlignment(Qt.AlignCenter)
        self.text.setMaximumWidth(28)

        self.slider.setRange(minValue, maxValue)
        self.slider.setValue(current)
        self.slider.setTickInterval(5)

        layout.addWidget(self.label)
        layout.addWidget(self.slider)
        layout.addWidget(self.text)

        layout.setStretchFactor(self.label, 1)
        layout.setStretchFactor(self.slider, 19)
        layout.setStretchFactor(self.text, 1)
        layout.setStretchFactor(self.text, 1)
        self.slider.valueChanged.connect(self._value)
        self.text.returnPressed.connect(self._update)

        self.setLayout(layout)

    def _value(self):
        a = self.sender()
        self.text.setText(str(a.value()))
        self.setValue(a.value())
        self.show()

    def _update(self):
        text = self.text.text()
        if isNumeric(text):
            self.slider.setValue(int(text))  # it will call _value too. so there's no need to call setValue()

    def refresh(self):
        if isinstance(self.current, int):
            return self.current
        else:
            current = self.current()

        self.text.setText(str(current))
        self.slider.setValue(current)
        return current

    def setMinMax(self, minValue, maxValue):
        self.slider.setRange(minValue, maxValue)


def test(c):
    print("I set the number of xxx as " + str(c))


if __name__ == '__main__':
    App = QApplication(sys.argv)
    window = MySlider("test1", test, -1)
    print(window.__doc__)
    window.show()
    sys.exit(App.exec())
