#! /usr/bin/env python3
# -*- coding:utf-8 -*-
from PyQt5.QtCore import QRectF

# Author by Alex

from geom.Element import Tile, Textile
from cfg import *
import utils

from PyQt5.QtGui import QPainterPath
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView


class Main(QGraphicsView):

    def __init__(self, parent, tiles=DEMO_TILES):
        super(Main, self).__init__(parent)

        # self.setMouseTracking(True)

        self.statbar = parent.statbar  # StatusBar
        self.textile = Textile(tiles)
        self.scene = QGraphicsScene()
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.drawColor = utils.makeColor(TILE_COLOR)  # Color of tile
        self.currentTile = None
        self.drawMap()
        self.setScene(self.scene)

    def createTile(self, config):
        idShape, paths, color = config.getConfig()
        item = Tile(self, idShape, paths, color)

        item.setBrush(utils.makeColor(item.color))

        path = QPainterPath()
        points = item.getPath()
        if len(points) == 0:
            raise "wrong item info"
        x, y = points[len(points) - 1]
        path.moveTo(x, y)
        for x, y in points:
            path.lineTo(x, y)

        path.closeSubpath()

        item.setPath(path)

        return item

    def drawMap(self):
        # clear screen
        self.scene.clear()

        # draw Grids
        if GRIDLINES:
            brush = QBrush()
            brush.setColor(utils.makeColor(GRID_COLOR))
            brush.setStyle(Qt.CrossPattern)  # Grid pattern.
            self.scene.setBackgroundBrush(brush)

        # draw Tiles
        for tile in self.textile.tiles:
            self.scene.addItem(self.createTile(tile))

    def addTile(self, coord, width, height, angle):
        self.textile.addTile(coord, width, height, angle, TILE_COLOR)
        self.drawMap()

    def updateTile(self, idShape, coord, width, height, angle):
        self.textile.updateTile(idShape, coord, width, height, angle)
        self.drawMap()

    # def mousePressEvent(self, event):
    #     pass
    #     # self.statbar.showMessage("Ready" + str(event.screenPos().x()) + ', ' + str(event.screenPos().y()))
    #     # self.drawColor = MyColor.CHOOSE.value
    #     # self.drawMap()
    #     # event.accept()
